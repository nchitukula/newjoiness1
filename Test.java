import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Implementing the class with all methods.
 * 
 * @author nchitukula
 *
 */
public class Test {
	static final String DB_URL = "jdbc:mysql://localhost/neha";
	static final String USER = "root";
	static final String PASS = "root";

	/**
	 * Helper method for creating Table In SQL with JDBC.
	 * 
	 */
	public void createTable() {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {
			String sql = "CREATE TABLE samosa " + "(username varchar(255) , " + "id int," + " otp varchar(255), "
					+ " recovery_code varchar(255) , " + " firstname varchar(255), " + " lastname varchar(255),"
					+ "department varchar(255)," + "location varchar(255))";

			stmt.executeUpdate(sql);
			System.out.println("Created table in given database...");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Helper method for reading the file and Writing into DataBase.
	 * 
	 * @throws IOException
	 */

	public void readFileAndWriteToDB() throws IOException {

		File file = new File("C:\\Users\\nchitukula\\neweclipseworkspace2\\Newjionees\\src\\newjionees.txt");

		BufferedReader br = new BufferedReader(new FileReader(file));
		try {
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			Statement stmt = conn.createStatement();
			String line = null;
			while ((line = br.readLine()) != null) {
				String splitReadLine[] = line.split(";");
				String username = "", id = "", otp = "", recovery_code = "", firstname = "", lastname = "",
						department = "", location = "";
				username = splitReadLine[0];
				id = splitReadLine[1];
				otp = splitReadLine[2];
				recovery_code = splitReadLine[3];
				firstname = splitReadLine[4];
				lastname = splitReadLine[5];
				department = splitReadLine[6];
				location = splitReadLine[7];

				String myQuery = "insert into samosa values('" + username + "'," + Integer.parseInt(id) + ",'" + otp
						+ "','" + recovery_code + "'," + "'" + firstname + "','" + lastname + "','" + department + "','"
						+ location + "')";
				// System.out.println(myQuery);
				stmt.executeUpdate(myQuery);

				System.out.println("data " + username + "added to table successfully");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			br.close();
		}
	}
}
