import java.io.IOException;

/**
 * Implementing the Main class.
 * 
 * @author nchitukula
 *
 */
public class Main {
	public static void main (String[] args) {
		Test test = new Test(); // object creation.
//		test.createTable();     // calling the method to create table.
		try {
			test.readFileAndWriteToDB();  // Calling the method to read the give data in a file.
		} catch (IOException e) {
			
			e.printStackTrace();
		}        
		// test.createTable();     // calling the method to create table.
		
	}

}
